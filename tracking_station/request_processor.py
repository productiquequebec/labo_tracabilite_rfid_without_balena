from abc import ABC, abstractmethod


class ReqType():
    START_POLLING = "start_polling"
    STOP_POLLING = "stop_polling"
    READ_TAG = "read_tag"
    WRITE_TAG = "write_tag"
    SET_PARAMETER = "set_parameter"
    STOP = "stop_reader"
    RESET = "reset_reader"


class RequestProcessorFactory:

    def new_processor(self, request_dict):
        """New request processor.

            Description   : Create a request processor of the specified 
                            type
            Parameters    : Request dictionnary.
            Return value  : Request processor.
        """
        type_ = request_dict["command"]

        if type_ == ReqType.START_POLLING:
            processor = StartPollingProcessor(request_dict)
        elif type_ == ReqType.STOP_POLLING:
            processor = StopPollingProcessor(request_dict)
        elif type_ == ReqType.READ_TAG:
            processor = ReadTagProcessor(request_dict)
        elif type_ == ReqType.WRITE_TAG:
            processor = WriteToTagProcessor(request_dict)
        elif type_ == ReqType.RESET:
            processor = ResetReaderProcessor(request_dict)
        else:
            raise SyntaxError("Invalid request")

        return processor

class AbstractRequestProcessor(ABC):

    def run(self, station):
        self.init(station)
        self.execute(station)
        return self.end(station)

    @abstractmethod
    def init(self, station):
        pass

    @abstractmethod
    def execute(self, station):
        pass

    @abstractmethod
    def end(self, station):
        pass


class StartPollingProcessor(AbstractRequestProcessor):

    def __init__(self, request_dict):
        pass

    def init(self, station):
        if station.rfid_reader.polling_running:
            raise RuntimeError("Polling already activated")

    def execute(self, station):
        ret = station.rfid_reader.start_polling()
        if ret is not None : raise RuntimeError(ret)

    def end(self, station):
        return 0, None


class StopPollingProcessor(AbstractRequestProcessor):

    def __init__(self, request_dict):
        pass

    def init(self, station):
        if not station.rfid_reader.polling_running:
            raise RuntimeError("Polling already stopped")

    def execute(self, station):
        ret = station.rfid_reader.stop_polling()
        if ret is not None: raise RuntimeError(ret)
        
    def end(self, station):
        return 0, None


class ReadTagProcessor(AbstractRequestProcessor):

    def __init__(self, request_dict):
        pass

    def init(self, station):
        if not station.rfid_reader.polling_running:
            ret = station.rfid_reader.start_polling()
            if ret is not None : raise RuntimeError(ret)

    def execute(self, station):
        try:
            self.tag = station.rfid_reader.read_tag()
        except Exception as e:
            raise RuntimeError(str(e))

    def end(self, station):
        return 0, self.tag


class WriteToTagProcessor(AbstractRequestProcessor):

    def __init__(self, request_dict):
        self.target = request_dict["target"]
        if not isinstance(self.target, str):
            raise SyntaxError("Target argument must be in string format")
        self.value = request_dict["value"]
        if not isinstance(self.target, str):
            raise SyntaxError("Value argument must be in string format")

    def init(self, station):
        if station.rfid_reader.polling_running == False:
            ret = station.rfid_reader.start_polling()
            if ret is not None : raise RuntimeError(ret)

    def execute(self, station):
        ret = station.rfid_reader.write_tag(self.target, self.value)
        if ret is not None: raise RuntimeError(str(ret))

    def end(self, station):
        return 0, None


class SetParameterProcessor(AbstractRequestProcessor):

    def __init__(self, request_dict):
        pass

    def init(self, station):
        self.target = request_dict["target"]
        self.value = request_dict["value"]

    def execute(self, station):
        ret = station.rfid_reader.set_parameter(self.target, self.value)
        if ret is not None: raise RuntimeError(str(ret))

    def end(self, station):
        return 0, None

class ResetReaderProcessor(AbstractRequestProcessor):

    def __init__(self, request_dict):
        pass

    def init(self, station):
        pass

    def execute(self, station):
        ret = station.rfid_reader.stop()
        if ret is not None: raise RuntimeError(str(ret))

        ret = station.rfid_reader.init()
        if ret is not None: raise RuntimeError(str(ret))

        ret = station.rfid_reader.start_polling()
        if ret is not None: raise RuntimeError(str(ret))

    def end(self, station):
        return 0, None