import os
import json
from abc import ABC, abstractmethod
# from balena import Balena


class ConfigKey:
    """
    Configuration keys. Make sure these correspond in the json
    configuration file and the Balena dashboard
    """
    MQTT_BROKER_IP = 'mqtt_broker_ip'
    MQTT_BROKER_PORT = 'mqtt_broker_port'
    MQTT_BROKER_SOCKET_TIMEOUT = 'mqtt_broker_socket_timeout'
    
    MQTT_CLIENT_ID = 'mqtt_client_id'
    MQTT_CLIENT_PASSWORD = 'mqtt_client_password'
    MQTT_CLIENT_KEEP_ALIVE = 'mqtt_client_keep_alive'
    MQTT_CLIENT_CALLBACK_TIMEOUT = 'mqtt_client_callback_timeout'
    
    MQTT_TOPIC_PREFIX_COMMAND = 'mqtt_topic_prefix_command'
    MQTT_TOPIC_PREFIX_METRIC = 'mqtt_topic_prefix_metric'
    MQTT_TOPIC_APPLICATION = 'mqtt_topic_application'
    MQTT_TOPIC_CONTEXT = 'mqtt_topic_context'
    MQTT_TOPIC_TYPE_REQUEST = 'mqtt_topic_type_request'
    MQTT_TOPIC_TYPE_RESPONSE = 'mqtt_topic_type_response'
    MQTT_TOPIC_TYPE_PALLET_TIMESTAMP = 'mqtt_topic_type_pallet_timestamp'
    MQTT_TOPIC_TYPE_CLIENT_STATE = 'mqtt_topic_type_client_state'

    LED_PIN_MQTT_STATE = 'led_pin_mqtt_state'
    LED_PIN_RFID_TAG = 'led_pin_rfid_tag'


class ConfigType:
    BALENA = 'balena'
    JSON = 'json'


class ConfiguratorFactory:

    def new_configurator(self, type_):
        if type_ == ConfigType.BALENA:
            return BalenaConfigurator()
        elif type_ == ConfigType.JSON:
            return JSONConfigurator()
        else:
            raise TypeError("Configurator type not available")


class AbstractConfigurator(ABC):
    def __init__(self):
        self.config_dict = None

    @abstractmethod
    def get_variables(self):
        pass


# class BalenaConfigurator(AbstractConfigurator):
#     def __init__(self):
#         self.config_dict = dict()
#         self.balena = Balena()
#         self.credentials = {'username':'laboratoireproductique@productique.quebec',
#                             'password':'Mob26195'}
#         self.balena.auth.login(**self.credentials)

#     def get_variables(self):
#         """Get Balena configuration variables

#             Description   : Read the Balena OS environment variable, use
#                             them to retrieve the configuration variables 
#                             from the API and parse the returned list
#             Parameters    : None.
#             Return value  : Configuration dictionnary.
#         """
        
#         device_uuid = os.environ['BALENA_DEVICE_UUID']
#         app_id = os.environ['BALENA_APP_ID']
        
#         app_variables = (self.balena.models.environment_variables.\
#                          application.get_all(app_id))
#         device_variables = (self.balena.models.environment_variables.\
#                             device.get_all(device_uuid))

#         for var in app_variables:
#             self.config_dict[var['name']] = var['value']
            
#         for var in device_variables:
#             self.config_dict[var['name']] = var['value']

#         return self.config_dict


class JSONConfigurator(AbstractConfigurator):
    def __init__(self):
        self.config_dict = None
        
    def get_variables(self):
        """Get JSON configuration variables

            Description   : Open the configuration file and load content
                            to a dictionnary.
            Parameters    : None.
            Return value  : Configuration dictionnary.
        """
        location = os.path.realpath(os.path.join(os.getcwd(), 
                            os.path.dirname(__file__)))
        with open(os.path.join(location, 'config.json'), 'r') as f:
            self.config_dict = json.load(f)

        return self.config_dict