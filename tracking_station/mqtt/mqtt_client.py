import socket
import queue
import threading

import paho.mqtt.client as paho

from tracking_station.gpio.led import LED
from tracking_station.utils.logger import Logger
from tracking_station.mqtt.mqtt_message import MQTT_MessageFactory
from tracking_station.mqtt.mqtt_message import MQTT_Topic
from tracking_station.mqtt.mqtt_message import MsgType
from tracking_station.config.config import ConfigKey

#Callback types
ON_CONNECT = "on_connect"
ON_DISCONNECT = "on_disconnect"
ON_SUBSCRIBE = "on_subscribe"
ON_PUBLISH = "on_publish"
ON_MESSAGE = "on_message"

#Client states
UNINITIALIZED = "uninitialized"
CONNECTED = "connected"
DISCONNECTED = "disconnected"
CONNECTION_INTERRUPTED = "connection_interrupted"

#Message keys
STATE = 'state'


class MQTT_Client:
    def __init__(self, config_dict, data_type, msg_template, data_queue, 
                    data_cv):

        self.config_dict = config_dict
        self.request_type = data_type
        self.request_msg = msg_template
        self.message_queue = data_queue
        self.new_msg_cv = data_cv

        self.client = paho.Client(
            client_id=self.config_dict[ConfigKey.MQTT_CLIENT_ID],
            protocol=paho.MQTTv311
        )
        socket.setdefaulttimeout(
            int(self.config_dict[ConfigKey.MQTT_BROKER_SOCKET_TIMEOUT]))

        self.state_led = LED(
            int(self.config_dict[ConfigKey.LED_PIN_MQTT_STATE]))
        self.state = None

        self.mqtt_message_factory = MQTT_MessageFactory(self.config_dict)
        
        self.mqtt_lock = threading.RLock()
        self.callback_timeout = self.callback_timeout = int(
            self.config_dict[ConfigKey.MQTT_CLIENT_CALLBACK_TIMEOUT])
        self.new_callback_cv = threading.Condition()
        self.callback_data = None
        self.callback_type = None
        
    def init(self):
        """Initialise MQTT Client.

        Description   : Create the client, set callbacks, and subscribe 
                        to required topics.
        Parameters    : None.
        Return value  : None if initialization is successful.
        """
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_subscribe = self.on_subscribe
        self.client.on_publish = self.on_publish
        self.client.on_message = self.on_message
        
        ret = self.set_last_will()
        if ret is not None : return ret

        self.client.loop_start()

        ret = self.connect_broker()
        if ret is not None : return ret

        return None

    def delete(self):
        if self.get_state() == CONNECTED:
            try: 
                self.disconnect_broker()
            except Exception as e: 
                return e

    def connect_broker(self):
        """Connect to MQTT Broker.

        Description   : Establish the connection with the MQTT Broker 
                        and wait for the CONNACK callback.
        Parameters    : None
        Return value  : None if connection is successful.
        """
        self.mqtt_lock.acquire()
        try: 
            self.client.connect(
                host= self.config_dict[ConfigKey.MQTT_BROKER_IP], 
                port= int(self.config_dict[ConfigKey.MQTT_BROKER_PORT]),
                keepalive= int(
                    self.config_dict[ConfigKey.MQTT_CLIENT_KEEP_ALIVE]))
            callback_data = self.get_callback(ON_CONNECT)

            if callback_data == paho.CONNACK_ACCEPTED:
                Logger.log().info("Connected to {}:{} successfully"\
                    .format(self.config_dict[ConfigKey.MQTT_BROKER_IP], 
                            self.config_dict[ConfigKey.MQTT_BROKER_PORT]))
            elif callback_data != paho.CONNACK_ACCEPTED:
                raise ConnectionError("Connection unsuccessful, result :{}"\
                    .format(callback_data)) 

            return None

        except Exception as e: 
            Logger.log().error(str(e))
            return e
        finally:
            self.mqtt_lock.release()

    def disconnect_broker(self):
        """Disconnect from MQTT Broker.

        Description   : Close the connection with the MQTT Broker and
                        wait for the callback.
        Parameters    : None.
        Return value  : None if disconnection is successful.
        """
        self.mqtt_lock.acquire()
        try: 
            self.publish_state(DISCONNECTED)
            self.client.disconnect()
            callback_data = self.get_callback(ON_DISCONNECT)
            if callback_data == paho.MQTT_ERR_SUCCESS:
                self.state_led.turn_off()
                Logger.log().info("Disconnected from {}:{} successfully"\
                    .format(self.config_dict[ConfigKey.MQTT_BROKER_IP], 
                            self.config_dict[ConfigKey.MQTT_BROKER_PORT]))
            else:
                raise ConnectionError("Error while disconnecting from {}:{}"\
                    .format(self.config_dict[ConfigKey.MQTT_BROKER_IP],
                            self.config_dict[ConfigKey.MQTT_BROKER_PORT]))
            return None
            
        except Exception as e:
            Logger.log().error(str(e))
            return e
        finally:
            self.mqtt_lock.release()

    def subscribe_topic(self, topic, qos):
        """Subscribe to MQTT topic.

        Description   : Subscribe to the topic and wait for the SUBACK
                        callback.
        Parameters    : Target topic and QoS.
        Return value  : None if subscription is successful.
        """
        self.mqtt_lock.acquire()
        try: 
            result, mid = self.client.subscribe(topic=topic, qos=qos)
            if result == paho.MQTT_ERR_NO_CONN:
                raise ConnectionError("Tried to subscribe while\
                                      client is disconnected")
            
            callback_data = self.get_callback(ON_SUBSCRIBE)
            if callback_data == qos:
                Logger.log().info(
                    "Subscribed to {} successfully".format(topic))
            else:
                raise Exception("Subscription unsuccessful, qos result :{}"\
                    .format(self.callback_data))
            return None

        except Exception as e: 
            Logger.log().error(str(e))
            return e
        finally:
            self.mqtt_lock.release()

    def publish_message(self, message):
        """Publish MQTT message.

        Description   : Build and publish the message, then wait for
                        the PUBACK callback.
        Parameters    : Message object.
        Return value  : None if publish is successful.
        """
        self.mqtt_lock.acquire()
        
        try:
            message.build_message()
            message_info = self.client.publish(topic=message.topic, 
                                               payload=message.payload, 
                                               qos=message.qos, 
                                               retain = message.retain)

            if message_info.rc == paho.MQTT_ERR_NO_CONN:
                raise ConnectionError("Tried to publish while client\
                                      is disconnected")
            elif message_info.rc == paho.MQTT_ERR_QUEUE_SIZE:
                raise Exception("Message queue full")
           
            callback_data = self.get_callback(ON_PUBLISH)
            if callback_data == message_info.mid:
                Logger.log().info(
                    "Published message with mid: {} successfully"\
                    .format(message_info.mid))
            else:
                raise Exception("Error while publishing message with mid :{}"\
                    .format(message_info.mid))
            return None

        except Exception as e:
            Logger.log().error(str(e))
            return e
        finally:
            self.mqtt_lock.release()

    def get_callback(self, type_):
        """Get MQTT callback data.

        Description   : Wait for a new callback and return the data.
        Parameters    : Callback type.
        Return value  : Callback data.
        """
        with self.new_callback_cv:
            ret = self.new_callback_cv.wait_for(lambda:
                self.callback_type == type_, timeout= self.callback_timeout)
            if ret == False:
                raise TimeoutError("Timed out waiting for {} callback"\
                    .format(type_))
            self.callback_type = None
            return self.callback_data

    def on_connect(self, client, userdata, flags, rc):
        self.new_callback_cv.acquire()
        self.callback_type = ON_CONNECT
        self.state_led.turn_on()
        #Publishing state in a separate thread to avoid deadlock
        self.publish_state(thread=True)
        self.callback_data = rc
        self.new_callback_cv.notify_all()
        self.new_callback_cv.release()

    def on_disconnect(self, client, userdata, rc):
        self.new_callback_cv.acquire()
        self.callback_type = ON_DISCONNECT
        self.state_led.turn_off()
        if rc != 0:
            Logger.log().error("Unexpected disconnection, result : {}"\
                .format(rc))
        self.callback_data = rc
        self.new_callback_cv.notify_all()
        self.new_callback_cv.release()

    def on_subscribe(self, client, userdata, mid, granted_qos):
        self.new_callback_cv.acquire()
        self.callback_type = ON_SUBSCRIBE
        self.callback_data = granted_qos[0]
        self.new_callback_cv.notify_all()
        self.new_callback_cv.release()
    
    def on_publish(self, client, userdata, mid):
        self.new_callback_cv.acquire()
        self.callback_type = ON_PUBLISH
        self.callback_data = mid
        self.new_callback_cv.notify_all()
        self.new_callback_cv.release()

    def on_message(self, client, userdata, msg):
        self.new_msg_cv.acquire()
        if msg.topic.startswith(self.config_dict[ConfigKey.MQTT_TOPIC_PREFIX_COMMAND]):
            queue_content = self.request_type, self.request_msg, msg
            self.message_queue.put(queue_content)
            self.new_msg_cv.notify_all()
        else:
            Logger.log().warning("Received message to {} that can't "
                                 "be processed".format(msg.topic))
        self.new_msg_cv.release()

    def set_last_will(self):
        """Sets last will message.

        Description   : Create the message object and send it to the 
                        broker.
        Parameters    : None.
        Return value  : None if action is successful.
        """
        self.mqtt_lock.acquire()
        try:
            state_dict = dict()
            state_dict[STATE] = CONNECTION_INTERRUPTED

            last_will_message = self.mqtt_message_factory\
                .new_message(MsgType.CLIENT_STATE)
            last_will_message.set_metric(state_dict)
            last_will_message.build_message()
    
            self.client.will_set(topic = last_will_message.topic, 
                                 payload = last_will_message.payload, 
                                 qos = last_will_message.qos, 
                                 retain = last_will_message.retain)
            return None
        except Exception as e:
            Logger.log().error(e) 
            return e
        finally:
            self.mqtt_lock.release()

    def get_state(self):
        state = self.client._state
        if state == paho.mqtt_cs_new:
            return UNINITIALIZED
        elif state == paho.mqtt_cs_connected:
            return CONNECTED
        elif state == paho.mqtt_cs_disconnecting:
            return DISCONNECTED
        else:
            return "unknown state"

    def publish_state(self, state = None, thread = False):
        """Publish client state.

        Description   : Create a message object, set the current state
                        and send it to the broker. It is possible to 
                        publish in a separate thread to avoid a 
                        deadlock.
        Parameters    : State (optional), thread.
        Return value  : None if publish is successful.
        """
        if state == None:
            state = self.get_state()

        state_dict = dict()
        state_dict[STATE] = state

        self.state_message = self.mqtt_message_factory\
            .new_message(MsgType.CLIENT_STATE)
        self.state_message.set_metric(state_dict)
        
        if thread:
            publish_thread = threading.Thread(target= self.publish_message, 
                                              args=[self.state_message])
            publish_thread.daemon = True
            publish_thread.start()
        else:
            ret = self.publish_message(self.state_message)
            if ret is not None : return ret

        return None