import json
from abc import ABC, abstractmethod
from datetime import datetime, timezone
from enum import Enum

from tracking_station.config.config import ConfigKey


class MsgType(Enum):
    REQUEST = 1
    RESPONSE = 2
    CLIENT_STATE = 3
    PALLET_TIMESTAMP = 4


class MQTT_Topic():
    def __init__(self):
        self.prefix = None
        self.application = None
        self.context = None
        self.client = None
        self.type = None

    def serialize(self):
        topic = "/".join([self.prefix, self.application,\
             self.context, self.client, self.type])
        return topic

    def deserialize(self, topic):
        subtopics = topic.split("/")
        self.prefix = subtopics[0]
        self.application = subtopics[1]
        self.context = subtopics[2]
        self.client = subtopics[3]
        self.type = subtopics[4]


class MQTT_MessageFactory:

    def __init__(self, config_dict):
        self.config_dict = config_dict

    def new_message(self, type_):
        if type_ == MsgType.REQUEST:
            return Request(self.config_dict)
        elif type_ == MsgType.RESPONSE:
            return Response(self.config_dict)
        elif type_ == MsgType.CLIENT_STATE:
            return ClientState(self.config_dict)
        elif type_ == MsgType.PALLET_TIMESTAMP:
            return PalletTimestamp(self.config_dict)
        else:
            raise TypeError("Invalid message type")


class MQTT_Message(ABC):

    TIME = "time"
    CLIENT_ID = "client_id"

    def __init__(self, config_dict):
        self.config_dict = config_dict

        self.topic_obj = MQTT_Topic()
        self.topic_obj.application = (
            self.config_dict[ConfigKey.MQTT_TOPIC_APPLICATION])
        self.topic_obj.context = (
            self.config_dict[ConfigKey.MQTT_TOPIC_CONTEXT])
        self.topic_obj.client = (
            self.config_dict[ConfigKey.MQTT_CLIENT_ID])
        self.topic = None

        self.payload_dict = dict()
        self.payload_dict[self.TIME] = None
        self.payload_dict[self.CLIENT_ID] = (
            self.config_dict[ConfigKey.MQTT_CLIENT_ID])
        self.payload = None

        self.qos = 0
        self.retain = False

    def serialize_topic(self):
        self.topic = self.topic_obj.serialize()

    def serialize_payload(self):
        self.payload_dict[self.TIME] = datetime.now(timezone.utc)\
            .astimezone().isoformat() 
        self.payload = json.dumps(self.payload_dict)
    
    @abstractmethod
    def build_message(self): 
        pass


class MQTT_CmdType(MQTT_Message):

    SESSION_ID = "session_id"

    def __init__(self, config_dict):
        super().__init__(config_dict)
        self.topic_obj.prefix = (
            self.config_dict[ConfigKey.MQTT_TOPIC_PREFIX_COMMAND])
        self.payload_dict[self.SESSION_ID] = None
        self.qos = 2
        self.retain = False
    
    def set_session_id(self, session_id):
        self.payload_dict[self.SESSION_ID] = session_id


class Request(MQTT_CmdType):

    RESPONSE_TOPIC = "response_topic"
    REQUEST = "request"

    keys = [MQTT_Message.TIME, MQTT_Message.CLIENT_ID, 
            MQTT_CmdType.SESSION_ID, RESPONSE_TOPIC, REQUEST]

    def __init__(self, config_dict):
        super().__init__(config_dict)
        self.topic_obj.type = (
            self.config_dict[ConfigKey.MQTT_TOPIC_TYPE_REQUEST])
        self.response_topic_obj = MQTT_Topic()
        self.response_topic_obj.prefix = (
            self.config_dict[ConfigKey.MQTT_TOPIC_PREFIX_COMMAND])
        self.response_topic_obj.application = (
            self.config_dict[ConfigKey.MQTT_TOPIC_APPLICATION])
        self.response_topic_obj.context = (
            self.config_dict[ConfigKey.MQTT_TOPIC_CONTEXT])
        self.response_topic_obj.client = (
            self.config_dict[ConfigKey.MQTT_CLIENT_ID])
        self.response_topic_obj.type = (
            self.config_dict[ConfigKey.MQTT_TOPIC_TYPE_RESPONSE])
        self.response_topic = self.response_topic_obj.serialize()
        self.payload_dict[self.RESPONSE_TOPIC] = self.response_topic
        
    def set_client(self, client):
        self.topic_obj.client = client

    def set_request(self, request):
        self.payload_dict[self.REQUEST] = request

    def build_message(self):
        self.serialize_topic()
        self.serialize_payload()


class Response(MQTT_CmdType):

    RESPONSE = "response"  
    RETURN_CODE = "return_code"
    ERR_MSG = "error_message"
    RETURN_DATA = "return_data"

    keys = [MQTT_Message.TIME, 
            MQTT_Message.CLIENT_ID, 
            MQTT_CmdType.SESSION_ID, 
            RESPONSE]

    def __init__(self, config_dict):
        super().__init__(config_dict)
        self.topic_obj.type = (
            self.config_dict[ConfigKey.MQTT_TOPIC_TYPE_RESPONSE])

    def set_topic(self, topic):
        self.topic = topic
        
    def set_response(self, response):
        self.payload_dict[self.RESPONSE] = response

    def build_message(self):
        self.serialize_payload()


class MQTT_MetricType(MQTT_Message):

    METRIC = "metric"
    NAME = "name"
    VALUE = "value"
    INFLUX_IDX = "influx_idx"

    keys = [MQTT_Message.TIME, MQTT_Message.CLIENT_ID, METRIC]

    def __init__(self, config_dict):
        super().__init__(config_dict)
        self.topic_obj.prefix = (
            self.config_dict[ConfigKey.MQTT_TOPIC_PREFIX_METRIC])

    def set_metric(self, metric):
        self.payload_dict[self.METRIC] = metric
        
        #Influx requires numeric value to register new data
        self.payload_dict[self.INFLUX_IDX] = 1 
        
    def build_message(self):
        self.serialize_topic()
        self.serialize_payload()

class ClientState(MQTT_MetricType):

    def __init__(self, config_dict):
        super().__init__(config_dict)
        self.topic_obj.type = (
            self.config_dict[ConfigKey.MQTT_TOPIC_TYPE_CLIENT_STATE])
        self.qos = 0
        self.retain = True

class PalletTimestamp(MQTT_MetricType):

    def __init__(self, config_dict):
        super().__init__(config_dict)
        self.topic_obj.type = (
            self.config_dict[ConfigKey.MQTT_TOPIC_TYPE_PALLET_TIMESTAMP])
        self.qos = 0
        self.retain = False



