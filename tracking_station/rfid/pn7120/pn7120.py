import os
import subprocess
import threading
import queue
import json
import random
import signal

from tracking_station.config.config import ConfigKey
from tracking_station.gpio.led import LED

current_location = os.path.realpath(os.path.join(os.getcwd(), 
                                    os.path.dirname(__file__)))

PN7120_API_PATH = os.path.join(current_location, 'pn7120_API')
API_TIMEOUT = 5

#General key list
ID_KEY = "id"
TYPE_KEY = "type"
DATA_KEY = "data"

#Command key list
REQUEST_KEY = "request"
COMMAND_KEY = "command"
CMD_INITIALIZE = "initialize"
CMD_DEINITIALIZE = "deinitialize"
CMD_START_POLLING = "start_polling"
CMD_STOP_POLLING = "stop_polling"
CMD_WRITE_TAG = "write_tag"
CMD_SET_PARAMETER = "set_parameter"
CMD_STOP = "stop"
TARGET_KEY = "target"
VALUE_KEY = "value"

#Response key list
RESPONSE_KEY = "response"
RETURN_CODE_KEY = "return_code"
ERROR_MESSAGE_KEY = "error_message"

#Tag info key list
TAG_INFO_KEY = "tag_info"
UID_KEY = "uid"
TEXT_CONTENT_KEY = "text_content"
PROTOCOL_KEY = "protocol"


class PN7120():

    def __init__(self, config_dict, data_type, msg_template, data_queue, 
                 data_cv):
        
        self.config_dict = config_dict
        self.data_type = data_type
        self.msg_template = msg_template
        self.tag_queue = data_queue
        self.new_tag_cv = data_cv
 
        self.pn7120_api = None
        self.subprocess_launched = False
        self.reponse_thread = None

        self.init_dn = False
        self.polling_running = False

        self.new_response_cv = threading.Condition()
        self.response_queue = queue.Queue()

        self.tag_led = LED(int(self.config_dict[ConfigKey.LED_PIN_RFID_TAG]))
        self.current_tag = dict()


    def init(self):
        """Initialize PN7120.

            Description   : Make sure the PN7120 sensor is connected and 
                            initialize the NFC stack.
            Parameters    : None
            Return value  : None if initialization is successful.
        """

        self.pn7120_api = subprocess.Popen([PN7120_API_PATH], 
                                           stdin = subprocess.PIPE,
                                           stdout = subprocess.PIPE)
        self.subprocess_launched = True
        self.reponse_thread = threading.Thread(target = self.get_reponse)
        self.reponse_thread.daemon = True
        self.reponse_thread.start()

        ret = self.send_request(CMD_INITIALIZE)
        if ret is not None : return ret

        self.init_dn = True
        return None

    def deinit(self):
        """Deinitialize PN7120.

            Description   : Deinitialize the NFC stack and 
                            PN7120 sensor.
            Parameters    : None.
            Return value  : None if deinitialization is successful.
        """
        ret = self.send_request(CMD_DEINITIALIZE)  
        if ret is not None : return ret

        self.init_dn = False
        return None

    def stop(self):
        """stop subprocess.

            Description   : Terminate the PN7120 API subprocess.
            Parameters    : None.
            Return value  : None if closing is successful.
        """
        if self.subprocess_launched:
            if self.pn7120_api.poll() == None:      
                ret = self.send_request(CMD_STOP)
                if ret is not None :
                    self.pn7120_api.terminate()
                    self.pn7120_api.send_signal(signal.SIGKILL)

                self.pn7120_api.wait()
                self.polling_running = False
                self.init_dn = False
            
        return None

    def set_parameter(self, target, value):
        """Set a RFID parameter.

            Description   : Set a parameter in the PN7120 API.
            Parameters    : Target parameter, value to write.
            Return value  : None if setting is successful.
        """
        ret = self.send_request(CMD_SET_PARAMETER, target, value)  
        if ret is not None : return ret

        return None
            
    def start_polling(self):
        """Start tag polling.

            Description   : Enable tag polling and discovery. 
                            Will update the current tag object.
            Parameters    : None.
            Return value  : None if start is successful.
        """
        ret = self.send_request(CMD_START_POLLING)  
        if ret is not None : return ret

        self.polling_running = True
        return None
        
    def stop_polling(self):
        """Stop tag polling

            Description   : Disable tag polling and discovery.
            Parameters    : None.
            Return value  : None if stop is successful.
        """
        ret = self.send_request(CMD_STOP_POLLING)  
        if ret is not None : return ret

        self.polling_running = False
        return None

    def write_tag(self, target, value):
        """Write to tag

            Description   : Write text data to the tag's NDEF field.
            Parameters    : Target tag, value to write.
            Return value  : 0 if write is successful.
        """
        ret = self.send_request(CMD_WRITE_TAG, target, value)  
        if ret is not None : return ret

        return None

    def send_request(self, command, target = None, value = None):
        """Send request to API.

            Description   : Send a request, wait for completion and 
                            retrieve return code and error message.
            Parameters    : request key, target (optional), 
                            value (optional).
            Return value  : 0 if request is successful.
        """
        msg_dict = dict()
        msg_dict[TYPE_KEY] = REQUEST_KEY

        data_dict = dict()
        data_dict[COMMAND_KEY] = command
        data_dict[TARGET_KEY] = target
        data_dict[VALUE_KEY] = value

        msg_dict[DATA_KEY] = data_dict

        json_msg = json.dumps(msg_dict)
        json_msg = json_msg+"\n"

        self.pn7120_api.stdin.write(json_msg.encode('utf-8'))
        self.pn7120_api.stdin.flush()

        with self.new_response_cv:
            ret = self.new_response_cv.wait_for(
                lambda: not self.response_queue.empty(), timeout=API_TIMEOUT)
            if not ret:
                return TimeoutError("Timed out waiting for response from "
                                    "the PN7120 API")
            response_dict = self.response_queue.get()
        
        if DATA_KEY in response_dict:
            if RETURN_CODE_KEY in response_dict[DATA_KEY]:
                if response_dict[DATA_KEY][RETURN_CODE_KEY] == 0:
                    return None
                else:
                    return response_dict[DATA_KEY][ERROR_MESSAGE_KEY]
            else:
                return "Response message does not contain a return value"
        else:
            return "Response message does not contain response data"
            
    def get_reponse(self):
        """Get response.

            Description   : Reads data from the subprocess's stdout
            Parameters    : None.
            Return value  : None.
        """
        for line in iter(self.pn7120_api.stdout.readline, b''):
            try:
                response_dict = json.loads(line)
            except json.JSONDecodeError as e:
                pass

                #raise SyntaxError("JSON Decode error. Error message: {}"\
                #    .format(e))
            else:
                if TYPE_KEY in response_dict:
                    if response_dict[TYPE_KEY] == RESPONSE_KEY:
                        self.new_response_cv.acquire()
                        self.response_queue.put(response_dict)
                        self.new_response_cv.notify_all()
                        self.new_response_cv.release()

                    elif response_dict[TYPE_KEY] == TAG_INFO_KEY:
                        self.new_tag_cv.acquire()
                        if response_dict[DATA_KEY] == "null":
                            self.current_tag = None
                            self.tag_led.turn_off()
                            
                        else:
                            self.current_tag = response_dict[DATA_KEY]
                            self.tag_led.turn_on()

                        queue_content = (self.data_type, 
                                        self.msg_template, 
                                        response_dict[DATA_KEY])
                        self.tag_queue.put(queue_content)

                        self.new_tag_cv.notify_all()
                        self.new_tag_cv.release()

          

