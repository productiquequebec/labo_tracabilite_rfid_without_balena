# README #

### What is this repository for? ###

* High level API of the NXP Linux NFC library
* Version 0.0.0
* [Productique Québec](www.productique.quebec)

### How do I get set up? ###

* Libraries required to run:
    * json-c (https://github.com/json-c/json-c.git) 
        - Documentation at http://json-c.github.io/json-c/
        - Installation procedure : http://json-c.github.io/json-c/json-c-0.15/doc/html/index.html
        - Add -ljson-c to Makefile

    * linux_libnfc-nci (https://github.com/NXPNFCLinux/linux_libnfc-nci.git)
        - Documentation included in repo
        - Installation procedure : https://community.nxp.com/t5/NXP-Designs-Knowledge-Base/Easy-set-up-of-NFC-on-Raspberry-Pi/ta-p/1099034
        - Add -lnfc_nci_linux to Makefile

* API input commands: (Elements of format **value** means request parameters)
    * Initialize the NFC reader
        - {"type":"request", "data":{"command":"initialize"}}

    * Deinitialize the NFC reader
        - {"type":"request", "data":{"command":"deinitialize"}}

    * Start tag polling. This will send automatically a tag info message when a new tag is read. It will also send a message with null data when the tag goes out of range
        - {"type":"request", "data":{"command":"start_polling"}}

    * Stop tag polling
        - {"type":"request", "data":{"command":"stop_polling"}}

    * Write data to a tag. The uid must match the uid currently seen by the reader
        - {"type":"request", "data":{"command":"write_tag", "target":"**tag_uid (string)**", "value":"**value to write (string)**"}}

    * Set a NFC reader parameter. This function is not yet defined. 
        - {"type":"request", "data":{"command":"set_parameter", "target":"**parameter name (string)**", "value":"**value to write (string)**"}}

    * Stop the application
        - {"type":"request", "data":{"command":"stop"}}

* Return messages
    * Command feedback
        - {"type":"response", "data":{"return_code": function return code(int), "error_message":"function error message (string)"}}

    * New tag in range
        - {"type":"tag_info", "data":{"uid":"tag uid (string)", "text_content":"tag ndef text content(string)", "protocol":tag communication protocol(int), "error_message":"error encountered while reading tag (string)"}}
        
    * Tag out of range
        - {"type":"tag_info", "data":"null"}

### Who do I talk to? ###

* Olivier Fournier - olivier.fournier@productique.quebec
* David Brodeur - david.brodeur@productique.quebec