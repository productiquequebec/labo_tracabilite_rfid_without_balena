#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <pthread.h>
#include <json-c/json_object.h>
#include <json-c/json_tokener.h>
#include "linux_nfc_api.h"

extern nfc_tag_info_t g_tagInfos;
extern nfcTagCallback_t g_TagCB;
extern pthread_cond_t polling_condition;
extern pthread_mutex_t mutex;
extern pthread_t polling_thread;
extern struct tag *current_tag;
extern struct tag tag_info;
extern bool poll_running;
extern bool tag_present;
extern bool init_dn;

struct tag
{
    char *uid;
    char *text_content;
    int protocol;
    char *err_msg;
};

struct jobj_output_tag
{
    json_object *jstruct;
    json_object *type;
    json_object *data_struct;
    json_object *uid;
    json_object *text_content;
    json_object *protocol;
    json_object *err_msg;
};

struct jobj_output_response
{
    json_object *jstruct;
    json_object *type;
    json_object *data_struct;
    json_object *return_code;
    json_object *err_msg;
};

struct jobj_input_request
{
    json_tokener *tokener;
    json_object *jstruct;
    json_object *type;
    json_object *data_struct;
    json_object *cmd;
    json_object *target;
    json_object *value;
};

struct parsed_input
{
    const char* type;
    struct data
    {
        const char* cmd;
        const char* target;
        const char* value;
    }data;
};

void set_error(char **err_msg, const char *text);
int set_parameter(const char* target, const char* value, char** err_msg);
int write_tag(const char* target, const char* value, char** err_msg);
void data_to_str(char** text_content, char** err_msg);
void uid_to_str(char** uid);
int send_tag();
int initialize(char** err_msg);
int deinitialize(char** err_msg);
void onTagArrival(nfc_tag_info_t *pTagInfo);
void onTagDeparture(void);
void *process_tags(void *vargp);
int stop_polling(char** err_msg);
int start_polling(char** err_msg);
int send_response(int return_code, char* err_msg);
int parse_json(char* json_in, struct parsed_input* parsed_in);
void reset_input_struct(struct parsed_input* parsed_in);
int process_requests();