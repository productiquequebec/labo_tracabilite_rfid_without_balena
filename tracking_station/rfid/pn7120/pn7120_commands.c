#include "pn7120_subprocess.h"

nfc_tag_info_t g_tagInfos;
nfcTagCallback_t g_TagCB;
pthread_cond_t processor_condition = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t processor_thread;
struct tag  *current_tag = NULL;
struct tag tag_info = {.uid = NULL,
                       .text_content = NULL,
                       .protocol = 0,
                       .err_msg = NULL};

bool poll_running = false;
bool tag_present = false;
bool init_dn = false;
bool tag_processor_state = false;

void set_tag_attr(char **tag_attr, const char* value){
    if(!tag_attr){
        free(*tag_attr);
    }
    *tag_attr = strdup(value);
}

void set_error(char **err_msg, const char *text){
    if(!err_msg){
        free(*err_msg);
    }
    *err_msg = strdup(text);
}

int set_parameter(const char* target, const char* value, char** err_msg){
    /*************************************************************************
    *   Set reader parameter.
    *
    *   Description     : ...
    *   Parameters      : Parameter pointer, value pointer, error message 
    *                     pointer.
    *   Return value    : 0 if successful, otherwise -1.
    **************************************************************************/
    set_error(err_msg, "function not yet defined");
    return -1;
}

int write_tag(const char* target, const char* value, char** err_msg){
    /*************************************************************************
    *   Write data to tag.
    *
    *   Description     : Verify input, format tag if necessary and write to 
    *                     NDEF field.
    *   Parameters      : Target pointer, value pointer, error message pointer.
    *   Return value    : 0 if successful, otherwise -1.
    **************************************************************************/
    if(poll_running == false){
        set_error(err_msg, "polling deactivated");
        return -1;
    }
    else if(!current_tag){
        set_error(err_msg,  "no tag available");
    }
    else if(target == NULL){
        set_error(err_msg, "no target specified");
        return -1;
    }
    else if(strcmp(target, current_tag->uid)!=0){
        set_error(err_msg, "target not reachable");
        return -1;
    }
    else if(value == NULL){
        set_error(err_msg, "no data specified");
        return -1;
    }
    
    ndef_info_t NDEFinfo;
    unsigned char NDEFMsg[100];
    unsigned int NDEFMsgLen = 0;
    int res;
    char* text_content = (char *) malloc(strlen(value));
    strcpy(text_content, value);
    
    res = ndef_createText("en", text_content, NDEFMsg, sizeof(NDEFMsg));
    if(res <= 0x00){
        set_error(err_msg, "Failed to build TEXT NDEF Message");
        return -1;
    }
    else{
        NDEFMsgLen = res;
    }	

    if(nfcTag_isNdef(g_tagInfos.handle, &NDEFinfo)==0){
        if(nfcTag_isFormatable(g_tagInfos.handle)){
            if(nfcTag_formatTag(g_tagInfos.handle) == 0x00){
                if(nfcTag_isNdef(g_tagInfos.handle, &NDEFinfo)==0){
                    set_error(err_msg, "Tag formating failed");
                    return -1;
                }
            }
            else{
                set_error(err_msg, "Tag formating failed");
                return -1;
            }
        }
        else{
            set_error(err_msg, "Tag is not formattable");
            return -1;
        }
    }

    if(nfcTag_writeNdef(g_tagInfos.handle, NDEFMsg, NDEFMsgLen) == 0x00){
        return 0;
    }
    else{
        set_error(err_msg, "Write failed");
        return -1;
    }
}

void ndef_to_str(char** text_content, char** err_msg){
    /*************************************************************************
    *   Convert NDEF data to string.
    *
    *   Description     : Check tag type, format if necessary and read text 
    *                     content.
    *   Parameters      : Text string pointer, error message pointer.
    *   Return value    : void.
    **************************************************************************/
    nfc_friendly_type_t lNDEFType = NDEF_FRIENDLY_TYPE_OTHER;
    ndef_info_t NDEFinfo;
    unsigned char* NDEFContent = NULL;
    char* text_buff = NULL;
    set_error(err_msg, "null");
    set_tag_attr(text_content, "null");
    
    int res = 0x00;
    res = nfcTag_isNdef(g_tagInfos.handle, &NDEFinfo);
    if(0x01 == res) {
        NDEFContent = malloc(
            NDEFinfo.current_ndef_length * sizeof(unsigned char));
        res = nfcTag_readNdef(g_tagInfos.handle, 
                              NDEFContent, 
                              NDEFinfo.current_ndef_length, 
                              &lNDEFType);

        if(lNDEFType == NDEF_FRIENDLY_TYPE_TEXT) {
            text_buff = malloc(res * sizeof(char) + 1);
            res = ndef_readText(NDEFContent, res, text_buff, res);
            if(0x00 <= res){
                text_buff[res] = '\0';
                set_tag_attr(text_content, text_buff);
                free(text_buff);
                free(NDEFContent);
            }
            else{
                set_error(err_msg, "Read NDEF Text Error") ;
            }
        }
        else {
            set_error(err_msg, "Not a NDEF Text record");
        }
    }
    else {
        set_error(err_msg, "Not a NDEF tag");
    }

    return;
}

void uid_to_str(char** uid){
    /*************************************************************************
    *   Convert tag UID to string.
    *
    *   Description     : Convert tag uid to string format
    *   Parameters      : UID string pointer.
    *   Return value    : 0.
    **************************************************************************/
    set_tag_attr(uid, "null");
    char *uid_buff = (char *) malloc((3*g_tagInfos.uid_length+1)*sizeof(char));
      
    for(int i = 0; i < g_tagInfos.uid_length; i++){
        sprintf(uid_buff + 3*i, "%02X ", (unsigned char) g_tagInfos.uid[i]);
    }
    uid_buff[strlen(uid_buff)-1] = '\0';
    set_tag_attr(uid, uid_buff);
    
    free(uid_buff);
    return;
}

int send_tag(void){
    /*************************************************************************
    *   Process and send tag data.
    *
    *   Description     : Create, fill and send JSON structure as JSON string 
    *                     to master
    *   Parameters      : void.
    *   Return value    : 0.
    **************************************************************************/
    struct jobj_output_tag tag = {.type = NULL,
                                  .uid = NULL,
                                  .text_content = NULL,
                                  .protocol = 0,
                                  .err_msg = NULL};
    
    tag.jstruct = json_object_new_object();
    tag.type = json_object_new_string("tag_info");
    json_object_object_add(tag.jstruct, "type", tag.type);
    
    if(!current_tag){
        tag.data_struct = json_object_new_string("null");
    }
    else{
        tag.data_struct = json_object_new_object();
        tag.uid = json_object_new_string(current_tag->uid);
        tag.text_content = json_object_new_string(current_tag->text_content);
        tag.protocol = json_object_new_int(current_tag->protocol);
        tag.err_msg = json_object_new_string(current_tag->err_msg);

        json_object_object_add(tag.data_struct, "uid", tag.uid);
        json_object_object_add(tag.data_struct, 
                               "text_content", 
                               tag.text_content);
        json_object_object_add(tag.data_struct, "protocol", tag.protocol);
        json_object_object_add(tag.data_struct, "error_message", tag.err_msg);
    }

    json_object_object_add(tag.jstruct, "data", tag.data_struct);

    fprintf(stdout, "%s\n", json_object_to_json_string(tag.jstruct));
    fflush(stdout);

    return 0;   
}

void update_tag(void){
    /*************************************************************************
    *   Update tag data.
    *
    *   Description     : Update current tag struct from nfc tag info fields
    *   Parameters      : void.
    *   Return value    : void.
    **************************************************************************/
    if(tag_present==true){
        current_tag = &tag_info;
        uid_to_str(&tag_info.uid);
        ndef_to_str(&tag_info.text_content, &tag_info.err_msg);
        tag_info.protocol = g_tagInfos.protocol;
    }

    else if(tag_present == false){
        current_tag = NULL;
    }

    return;
}

void onTagArrival(nfc_tag_info_t *pTagInfo){
    /*************************************************************************
    *   Tag arrival callback.
    *
    *   Description     : Trigerred on tag arrival. Notifies polling thread.
    *   Parameters      : Tag data structure.
    *   Return value    : void.
    **************************************************************************/ 
    g_tagInfos = *pTagInfo;
    tag_present=true;
    pthread_cond_signal(&processor_condition);
}

void onTagDeparture(void){
    /*************************************************************************
    *   Tag departure callback.
    *
    *   Description     : Trigerred on tag departure. Notifies polling thread.
    *   Parameters      : None.
    *   Return value    : void.
    **************************************************************************/ 
    tag_present=false;
    pthread_cond_signal(&processor_condition);
}

void *process_tags(void *vargp){
    /*************************************************************************
    *   Tag polling function.
    *
    *   Description     : Wait for tags and send data.
    *   Parameters      : None.
    *   Return value    : void.
    **************************************************************************/ 
    while(tag_processor_state==true){
        pthread_cond_wait(&processor_condition, &mutex);
        if(poll_running==true){
            update_tag();
            send_tag();
        }
    }
    return 0;
}

int stop_polling(char **err_msg){
    /*************************************************************************
    *   Stop tag polling.
    *
    *   Description     : Disable discovery and disable polling.
    *   Parameters      : Error message pointer.
    *   Return value    : 0 if polling stopped successfully, otherwise -1.
    **************************************************************************/
    if(poll_running == true){
        poll_running = false;
        nfcManager_disableDiscovery();
        nfcManager_deregisterTagCallback();
    }
    else if(poll_running == false){
        set_error(err_msg, "polling not running");
        return -1;
    }
    return 0;
}

int start_polling(char **err_msg){
    /*************************************************************************
    *   Start tag polling.
    *
    *   Description     : Set nfcManager parameters and enable polling.
    *   Parameters      : Error message pointer.
    *   Return value    : 0 if polling started successfully, otherwise -1.
    **************************************************************************/
    if(init_dn == true){
        if(poll_running == false){
            g_TagCB.onTagArrival = onTagArrival;
            g_TagCB.onTagDeparture = onTagDeparture;
            nfcManager_registerTagCallback(&g_TagCB);
            nfcManager_enableDiscovery(DEFAULT_NFA_TECH_MASK, 0x01, 0, 0);
            poll_running=true;
        }
        else if(poll_running == true){
            set_error(err_msg, "polling already running");
            return -1;
        }
    }
    else if(init_dn == false){
        set_error(err_msg, "reader not yet initialized");
        return -1;
    }

    return 0;
}

int deinitialize(char **err_msg){
    /*************************************************************************
    *   Deinitialize NFC sensor.
    *
    *   Description     : Deinitializes nfc stack and clears current tag.
    *   Parameters      : Error message pointer.
    *   Return value    : 0 if successful, otherwise -1.
    **************************************************************************/ 
    if(poll_running == false){
        if(init_dn == true){
            tag_processor_state = false;
            pthread_cond_signal(&processor_condition);

            int ret = 0;
            ret = pthread_join(processor_thread, NULL);
            if(ret !=0){
                set_error(err_msg, "could not stop polling");
                return -1;
            }

            free(tag_info.uid);
            free(tag_info.text_content);
            free(tag_info.err_msg);
            init_dn = false;

            ret = nfcManager_doDeinitialize();
            if(ret != 0){
                set_error(err_msg, "deinitialization failed");
                return -1;
            }
        }
    }
    else{
        set_error(err_msg, "polling must be stopped first");
        return -1;
    }
    return 0;
}

int initialize(char **err_msg){
    /*************************************************************************
    *   Initialize NFC sensor.
    *
    *   Description     : Initializes nfc stack and current tag.
    *   Parameters      : Error message pointer.
    *   Return value    : 0 if successful, otherwise -1.
    **************************************************************************/ 
    if(init_dn == false){

        int ret = 0;
        ret = nfcManager_doInitialize();
        if(ret != 0){
            set_error(err_msg, "initialization failed");
            return -1;
        }
        else
        {
            set_tag_attr(&tag_info.uid, "null");
            set_tag_attr(&tag_info.text_content, "null");
            tag_info.protocol = 0;
            set_error(&tag_info.err_msg, "null");

            tag_processor_state = true;
            ret = pthread_create(&processor_thread, NULL, process_tags, NULL);
            if(ret !=0){
                set_error(err_msg, "could not start polling thread");
                return -1;
            }

            init_dn = true;
        }   
    }
    else{
        set_error(err_msg, "initialization already complete");
        return -1;
    }
    return 0;
}

int send_response(int return_code, char* err_msg){
    /*************************************************************************
    *   Send response.
    *
    *   Description     : Send response to master as JSON string.
    *   Parameters      : Return code and error message ptr.
    *   Return value    : 0.
    **************************************************************************/
    struct jobj_output_response jobj_out = {.type = NULL, 
                                            .return_code = 0, 
                                            .err_msg = NULL};

    jobj_out.jstruct = json_object_new_object();

    jobj_out.type = json_object_new_string("response");
    json_object_object_add(jobj_out.jstruct, "type", jobj_out.type);

    jobj_out.data_struct = json_object_new_object();

        jobj_out.return_code = json_object_new_int(return_code);
        json_object_object_add(jobj_out.data_struct, 
                               "return_code", 
                               jobj_out.return_code);
        
        jobj_out.err_msg = json_object_new_string(err_msg);
        json_object_object_add(jobj_out.data_struct, 
                               "error_message", 
                               jobj_out.err_msg);

    json_object_object_add(jobj_out.jstruct, "data", jobj_out.data_struct);
    
    fprintf(stdout, "%s\n", json_object_to_json_string(jobj_out.jstruct));
    fflush(stdout);

    return 0;
}

int parse_json(char* json_in, struct parsed_input* parsed_in){
    /*************************************************************************
    *   Parse JSON string.
    *
    *   Description     : Parse string to JSON object array and then 
    *                     to a usable structure.
    *   Parameters      : Input string ptr, parsed input structure ptr.
    *   Return value    : -1 if invalid JSON string, otherwise 0.
    **************************************************************************/
    struct jobj_input_request jobj_in = {.tokener = NULL, 
                                         .jstruct = NULL, 
                                         .type = NULL, 
                                         .data_struct = NULL, 
                                         .cmd = NULL, 
                                         .target = NULL, 
                                         .value = NULL};
    int len = strlen(json_in)+1;
    jobj_in.tokener = json_tokener_new();
    jobj_in.jstruct = json_tokener_parse_ex(jobj_in.tokener, json_in, len);
    json_tokener_free(jobj_in.tokener); 

    if(!jobj_in.jstruct){
        return -1;
    }

    if(json_object_object_get_ex(jobj_in.jstruct, "type", &jobj_in.type)){
        parsed_in->type = json_object_get_string(jobj_in.type);}
    if(json_object_object_get_ex(jobj_in.jstruct, "data", 
                                 &jobj_in.data_struct)){
        if(json_object_object_get_ex(jobj_in.data_struct, 
                                     "command", 
                                     &jobj_in.cmd)){
            parsed_in->data.cmd = json_object_get_string(jobj_in.cmd);}

        if(json_object_object_get_ex(jobj_in.data_struct, "target", 
                                     &jobj_in.target)){
            parsed_in->data.target = json_object_get_string(jobj_in.target);}

        if(json_object_object_get_ex(jobj_in.data_struct, "value", 
                                     &jobj_in.value)){
            parsed_in->data.value = json_object_get_string(jobj_in.value);}
    }
    return 0;
}

void reset_input_struct(struct parsed_input* parsed_in){
    parsed_in->type = NULL;
    parsed_in->data.cmd = NULL;
    parsed_in->data.target = NULL;
    parsed_in->data.value = NULL;
}
