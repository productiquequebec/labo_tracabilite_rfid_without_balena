#include "pn7120_subprocess.h"

int process_requests(){
    /*************************************************************************
    *   Process incoming requests.
    *
    *   Description     : Parse request, start function and send response to 
    *                     master.
    *   Parameters      : None.
    *   Return value    : 0.
    **************************************************************************/
    struct parsed_input parsed_in = { .type = NULL, 
                                      .data.cmd = NULL,
                                      .data.target = NULL,
                                      .data.value = NULL};
    char *err_msg = NULL;
    char *json_in = NULL;
    size_t json_len = 0;
    size_t nread = 0;
    
    int ret = 0;
    bool exit = false;

    do{
        set_error(&err_msg, "null");
        reset_input_struct(&parsed_in);

        nread = getline(&json_in, &json_len, stdin);
        json_in[nread-1] = '\0';
        
        ret = parse_json(json_in, &parsed_in);
        if(ret == 0){

            if(!parsed_in.type){
                ret = -3;
                set_error(&err_msg, "no type specified");
            }
            else if(strcmp(parsed_in.type, "request")==0){
                if(!parsed_in.data.cmd){
                    ret = -2;
                    set_error(&err_msg, "no command specified");
                }
                else if(strcmp(parsed_in.data.cmd, "initialize")==0){
                    ret = initialize(&err_msg);
                }
                else if(strcmp(parsed_in.data.cmd, "deinitialize")==0){
                    ret = deinitialize(&err_msg);
                }
                else if(strcmp(parsed_in.data.cmd, "start_polling")==0){
                    ret = start_polling(&err_msg);
                }
                else if(strcmp(parsed_in.data.cmd, "stop_polling")==0){
                    ret = stop_polling(&err_msg);
                }
                else if(strcmp(parsed_in.data.cmd, "write_tag")==0){ 
                    ret = write_tag(parsed_in.data.target, 
                                    parsed_in.data.value, 
                                    &err_msg);
                }
                else if(strcmp(parsed_in.data.cmd, "set_parameter")==0){
                    ret = set_parameter(parsed_in.data.target, 
                                        parsed_in.data.value, 
                                        &err_msg);
                }
                else if (strcmp(parsed_in.data.cmd, "stop")==0){
                    ret = 0;
                    set_error(&err_msg, "null");
                    exit = true;   
                }
                else{
                    ret = -2;
                    set_error(&err_msg, "command not supported");
                }
            }
            else{
                ret = -3;
                set_error(&err_msg, "type not supported");
            }
        }
        else{
            ret = -4;
            set_error(&err_msg, "could not parse json string");
        }

        send_response(ret, err_msg);
        if(!json_in){
            free(json_in);
        }

    }while(exit==false);
    
    free(err_msg);

    return 0;
}

int main(int argc, char* args[]){
    return process_requests();
}

