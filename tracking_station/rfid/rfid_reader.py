import threading

from tracking_station.rfid.pn7120.pn7120 import PN7120
from tracking_station.utils.logger import Logger
from tracking_station.config.config import ConfigKey


class RFID_ReaderAdapter:
    
    def __init__(self, config_dict, data_type, msg_template, 
                 data_queue, data_cv):

        self.rfid_adaptee = PN7120(config_dict,
                                   data_type, 
                                   msg_template, 
                                   data_queue, 
                                   data_cv)
        self.init_dn = False
        self.polling_running = False

    def delete(self):
        self.stop()
        
    def init(self):
        """Initialize RFID reader.

            Description   : Initialize the RFID reader.
            Parameters    : None.
            Return value  : None if initialization is successful.
        """

        ret = self.rfid_adaptee.init()
        if ret is not None: 
            Logger.log().error(str(ret))
            return ret
        
        self.init_dn = self.rfid_adaptee.init_dn
        Logger.log().info("Initialization successful")
        return None

    def deinit(self):
        """Deinitialize RFID reader.

            Description   : Deinitialize the RFID reader
            Parameters    : None.
            Return value  : None if deinitialization is successful.
        """
        ret = self.rfid_adaptee.deinit()
        if ret is not None: 
            Logger.log().error(str(ret))
            return ret

        self.init_dn = self.rfid_adaptee.init_dn
        Logger.log().info("Deinitialization successful")
        return None

    def stop(self):
        """Stop RFID Reader.

            Description   : Stop the sensor subprocess.
            Parameters    : None.
            Return value  : None if stopping is successful.
        """
        ret = self.rfid_adaptee.stop()
        if ret is not None: 
            Logger.log().error(str(ret))
            return ret

        Logger.log().info("Subprocess stopping successful")
        return None     

    def set_parameter(self, parameter, value):
        """Set parameter.

            Description   : Set a RFID sensor parameter.
            Parameters    : Parameter, value.
            Return value  : None if setting is successful.
        """
        ret = self.rfid_adaptee.set_parameter(parameter, value)
        if ret is not None: 
            Logger.log().error(str(ret))
            return ret

        Logger.log().info("Parameter setting successful")
        return None  

    def start_polling(self):
        """Start RFID polling.

            Description   : Start tag discovery and queueing.
            Parameters    : None.
            Return value  : None if start is successful.
        """
        ret = self.rfid_adaptee.start_polling()
        if ret is not None: 
            Logger.log().error(str(ret))
            return ret
        
        self.polling_running = self.rfid_adaptee.polling_running    
        Logger.log().info("Polling started successfully")
        return None

    def stop_polling(self):
        """Stop RFID polling.

            Description   : Stops and joins the RFID polling thread.
            Parameters    : None.
            Return value  : None if stop is successful.
        """
        ret = self.rfid_adaptee.stop_polling()
        if ret is not None: 
            Logger.log().error(str(ret))
            return ret
        
        self.polling_running = self.rfid_adaptee.polling_running
        Logger.log().info("Polling stopped successfully")
        return None  

    def read_tag(self):
        """Read current tag.

            Description   : Read the tag currently seen by the reader. 
                            If no tag present : null.
            Parameters    : None.
            Return value  : RFID tag object.
        """
        return self.rfid_adaptee.current_tag

    def write_tag(self, target, value):
        """Write to current tag.

            Description   : Write data to the current tag.
            Parameters    : Tag UID target, value to write.
            Return value  : None if write is successful.
        """
        ret = self.rfid_adaptee.write_tag(target, value)
        if ret is not None: 
            Logger.log().error(str(ret))
            return ret

        Logger.log().info("Written to tag successfully")
        return None