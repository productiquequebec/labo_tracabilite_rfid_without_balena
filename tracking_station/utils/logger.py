import logging

class Logger():
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s][%(module)s] In %(funcName)s, line no %(lineno)d : %(message)s')

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(formatter)

    file_handler = logging.FileHandler("tracking_station/logs/logging.log", mode='w')
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)

    logger = logging.getLogger("logger")
    logger.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)

    terminal_logger = logging.getLogger("terminal")
    terminal_logger.setLevel(logging.DEBUG)
    terminal_logger.addHandler(stream_handler)
    
    def __init__(self):
        pass

    @classmethod
    def log(cls): 
        return cls.terminal_logger

    @classmethod
    def terminal(cls):
        return cls.terminal_logger