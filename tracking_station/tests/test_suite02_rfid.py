import unittest
import time
import threading
import queue

from tracking_station.rfid.rfid_reader import RFID_ReaderAdapter
from tracking_station.config.config import ConfiguratorFactory
from tracking_station.config.config import ConfigType
from tracking_station.station import DataType
from tracking_station.mqtt.mqtt_message import MQTT_MetricType
from tracking_station.gpio.led import PiGPIO


class TestCase_RFID_Reader(unittest.TestCase):
    
    @classmethod
    def setUpClass(self):
        self.gpio = PiGPIO()
        self.data_cv = threading.Condition()
        self.data_queue = queue.Queue()

        configurator = ConfiguratorFactory.new_configurator(
            self, ConfigType.JSON)
        config_dict = configurator.get_variables()

        self.rfid_reader = RFID_ReaderAdapter(config_dict, 
                                              DataType.OUTPUT_METRIC, 
                                              MQTT_MetricType, 
                                              self.data_queue, 
                                              self.data_cv)

    @classmethod
    def tearDownClass(self):
        self.rfid_reader.delete()

    def test01_init(self):
        ret = self.rfid_reader.init()
        self.assertIsNone(ret)

    def test02_start_polling(self):
        ret = self.rfid_reader.start_polling()
        self.assertIsNone(ret)

    #Use the NXP NFC card for this test
    def test03_read(self):
        time.sleep(1)

        ret = self.rfid_reader.read_tag()
        self.assertEqual(ret['uid'], "04 4A 67 D2 9C 39 81")

    #Use the NXP NFC card for this test
    def test04_write(self):
        ret = self.rfid_reader.write_tag("04 4A 67 D2 9C 39 81", "test_rfid")
        self.assertIsNone(ret)

    def test05_stop_polling(self):
        ret = self.rfid_reader.stop_polling()
        self.assertIsNone(ret)

    def test06_stop(self):
        ret = self.rfid_reader.stop()
        self.assertIsNone(ret)


