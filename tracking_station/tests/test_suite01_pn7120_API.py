import unittest
import threading
import queue

from tracking_station.rfid.pn7120.pn7120 import PN7120
from tracking_station.config.config import ConfiguratorFactory
from tracking_station.config.config import ConfigType
from tracking_station.station import DataType
from tracking_station.mqtt.mqtt_message import MQTT_MetricType
from tracking_station.gpio.led import PiGPIO


class TestCase_PN7120_API(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.gpio = PiGPIO()
        self.data_cv = threading.Condition()
        self.data_queue = queue.Queue()

        configurator = ConfiguratorFactory.new_configurator(
            self, ConfigType.JSON)
        config_dict = configurator.get_variables()

        self.pn7120 = PN7120(config_dict, 
                             DataType.OUTPUT_METRIC, 
                             MQTT_MetricType, 
                             self.data_queue, 
                             self.data_cv)

    def test01_init(self):
        ret = self.pn7120.init()
        self.assertIsNone(ret)
    
    def test02_start_polling(self):
        ret = self.pn7120.start_polling()
        self.assertIsNone(ret)

    def test03_stop_polling(self):
        ret = self.pn7120.stop_polling()
        self.assertIsNone(ret)

    def test04_deinit(self):
        ret = self.pn7120.deinit()
        self.assertIsNone(ret)
    
    def test05_stop(self):
        ret = self.pn7120.stop()
        self.assertIsNone(ret)    

