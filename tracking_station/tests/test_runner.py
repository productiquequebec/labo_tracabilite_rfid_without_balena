import unittest
import os
import sys

unittest.TestLoader.sortTestMethodsUsing = None

def suite():
    loader = unittest.TestLoader()
    suite = loader.discover(os.path.dirname(__file__))
    return suite

def run_tests():
    runner = unittest.TextTestRunner()
    runner.run(suite())

