import unittest
import threading
import queue

from tracking_station.mqtt.mqtt_client import MQTT_Client
from tracking_station.mqtt.mqtt_message import MsgType
from tracking_station.config.config import ConfiguratorFactory
from tracking_station.config.config import ConfigType
from tracking_station.station import DataType
from tracking_station.mqtt.mqtt_message import MQTT_MetricType
from tracking_station.gpio.led import PiGPIO

class TestCase_MQTT_Client(unittest.TestCase):

    @classmethod
    def setUpClass(self):

        self.gpio = PiGPIO()
        self.data_cv = threading.Condition()
        self.data_queue = queue.Queue()

        configurator = ConfiguratorFactory.new_configurator(
            self, ConfigType.JSON)
        config_dict = configurator.get_variables()

        self.mqtt_client = MQTT_Client(config_dict, 
                                       DataType.INPUT_REQUEST, 
                                       MQTT_MetricType, 
                                       self.data_queue, 
                                       self.data_cv)
    @classmethod
    def tearDownClass(self):
        self.mqtt_client.delete()

    def test01_init(self):
        ret = self.mqtt_client.init()
        self.assertIsNone(ret)

    def test02_subscribe(self):
        ret = self.mqtt_client.subscribe_topic(
            "cmd/lab/traceability/st-dev/req", 2)
        self.assertIsNone(ret)

    def test03_publish_state(self):
        ret = self.mqtt_client.publish_state("test_state")
        self.assertIsNone(ret)

    def test04_disconnect_broker(self):
        ret = self.mqtt_client.disconnect_broker()
        self.assertIsNone(ret)
