import RPi.GPIO as GPIO


class PiGPIO:
    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)

    def delete(self):
        GPIO.cleanup()

   
class LED:
    def __init__(self, pin):
        self.led_pin = pin
        GPIO.setup(pin, GPIO.OUT, initial=GPIO.LOW)

    def turn_on(self):
        GPIO.output(self.led_pin, 1)

    def turn_off(self):
        GPIO.output(self.led_pin, 0)