import threading
import json
import queue
import re
from enum import Enum

from tracking_station.mqtt.mqtt_client import MQTT_Client
from tracking_station.mqtt.mqtt_message import MQTT_Topic
from tracking_station.mqtt.mqtt_message import MsgType
from tracking_station.mqtt.mqtt_message import Response
from tracking_station.mqtt.mqtt_message import Request

from tracking_station.rfid.rfid_reader import RFID_ReaderAdapter
from tracking_station.request_processor import RequestProcessorFactory

from tracking_station.config.config import ConfiguratorFactory
from tracking_station.config.config import ConfigType
from tracking_station.config.config import ConfigKey
from tracking_station.gpio.led import PiGPIO 

from tracking_station.utils.logger import Logger


class DataType(Enum):
    INPUT_REQUEST = 1
    OUTPUT_METRIC = 2


class TrackingStation:
    def __init__(self, config_type):
        self.data_queue = queue.Queue()
        self.data_cv = threading.Condition()
        self.request_processor_factory = RequestProcessorFactory()

        self.configurator_factory = ConfiguratorFactory()
        self.configurator = self.configurator_factory.new_configurator(
            config_type)
        self.config_dict = self.configurator.get_variables()

        self.gpio = PiGPIO()

        self.mqtt_client = MQTT_Client(
            config_dict = self.config_dict,
            data_type = DataType.INPUT_REQUEST, 
            msg_template = MsgType.REQUEST, 
            data_queue = self.data_queue, 
            data_cv = self.data_cv)

        self.rfid_reader = RFID_ReaderAdapter(
            config_dict = self.config_dict,
            data_type = DataType.OUTPUT_METRIC, 
            msg_template = MsgType.PALLET_TIMESTAMP, 
            data_queue = self.data_queue, 
            data_cv = self.data_cv)

    def init(self):
        """Initialize tracking station.

            Description   : Initialize the mqtt client and rfid reader.
            Parameters    : None.
            Return value  : None.
        """
        ret = self.mqtt_client.init()
        if ret is not None : return ret

        ret = self.rfid_reader.init()
        if ret is not None : return ret
        
        return None

    def delete(self):
        self.rfid_reader.delete()
        self.mqtt_client.delete()
        self.gpio.delete()

    def run(self):
        """Run tracking station.

            Description   : Main application loop. Start polling and 
                            wait for new data to process, then choose 
                            the appropriate way to process that data. 
            Parameters    : None.
            Return value  : None.
        """

        request_topic_obj = MQTT_Topic()
        request_topic_obj.prefix = (
            self.config_dict[ConfigKey.MQTT_TOPIC_PREFIX_COMMAND])
        request_topic_obj.application = (
            self.config_dict[ConfigKey.MQTT_TOPIC_APPLICATION])
        request_topic_obj.context = (
            self.config_dict[ConfigKey.MQTT_TOPIC_CONTEXT])
        request_topic_obj.client = (
            self.config_dict[ConfigKey.MQTT_CLIENT_ID])
        request_topic_obj.type = (
            self.config_dict[ConfigKey.MQTT_TOPIC_TYPE_REQUEST])
        request_topic = request_topic_obj.serialize()

        ret = self.mqtt_client.subscribe_topic(request_topic, 2)
        if ret is not None : return ret


        ret = self.rfid_reader.start_polling()
        if ret is not None : return ret

        while(True): 
            with self.data_cv:
                self.data_cv.wait_for(lambda: not self.data_queue.empty())
                data_type, msg_template, data = self.data_queue.get()

                if data_type == DataType.INPUT_REQUEST:
                    self.process_request(msg_template, data)
                elif data_type == DataType.OUTPUT_METRIC:
                    self.send_metric(msg_template, data)
                else:
                    Logger.log().error("Incorrect data type, could not be"
                                       "processed: {}".format(data))

    def process_request(self, request_template, request_msg):
        """Process MQTT request.

            Description   : Parse the message, create and execute the 
                            processor and send the response to the 
                            specified topic.
            Parameters    : Request message template, request message.
            Return value  : None.
        """
        Logger.log().info("Received new request")
        try:
            #Message parsing
            try:
                payload_dict = json.loads(request_msg.payload)
            except json.JSONDecodeError:
                #Try recovering message data to send error message
                payload_str = request_msg.payload.decode("utf-8")
                payload_words = re.findall("[/\w]+|[\w']+", payload_str)

                if Request.SESSION_ID in payload_words:
                    session_id_idx = payload_words.index(Request.SESSION_ID)
                    session_id = payload_words[session_id_idx+1]
                else:
                    session_id = "unknown"
                
                if Request.RESPONSE_TOPIC in payload_words:
                    response_topic_idx = payload_words.index(
                        Request.RESPONSE_TOPIC)
                    response_topic = payload_words[response_topic_idx+1]
                else:
                    response_topic = "cmd/error"

                raise SyntaxError("JSON Decode Error")

            else:

                if Request.SESSION_ID in payload_dict:
                    session_id = payload_dict[Request.SESSION_ID]
                else:
                    session_id = "unknown"

                if Request.RESPONSE_TOPIC in payload_dict:
                    response_topic = payload_dict[Request.RESPONSE_TOPIC]
                else: 
                    response_topic = "cmd/error"
                
                client_id = payload_dict[Request.CLIENT_ID]
                request_dict = payload_dict[Request.REQUEST]

            #Request processing
                processor = self.request_processor_factory.new_processor(
                    request_dict)
                return_code, return_data = processor.run(self)

        except KeyError as e:
            return_code = -1
            err_msg = ("Missing required key :{}".format(str(e)))
        except SyntaxError as e:
            return_code = -1
            err_msg = str(e)
        except RuntimeError as e:
            return_code = -2
            err_msg = str(e)
        except Exception as e:
            return_code = -3
            err_msg = str(e)
        finally:
            if return_code == 0:
                err_msg = None
                Logger.log().info("Command returned with code :{}"
                                  .format(return_code))
            else:
                return_data = None
                Logger.log().error("Command returned with code: {} and error "
                                   "message: {}".format(return_code, err_msg))

        #Send response
        try:
            response_msg = self.mqtt_client.mqtt_message_factory.new_message(
                MsgType.RESPONSE)

            response_dict = dict()
            response_dict[response_msg.RETURN_CODE] = return_code
            response_dict[response_msg.ERR_MSG] = err_msg
            response_dict[response_msg.RETURN_DATA] = return_data

            response_msg.set_session_id(session_id)
            response_msg.set_response(response_dict)
            response_msg.set_topic(response_topic)
            self.mqtt_client.publish_message(response_msg)
        except Exception as e:
            Logger.log().error(str(e))

    def send_metric(self, metric_template, metric_dict):
        """Send MQTT metric.

            Description   : Create and publish the metric message.
            Parameters    : Metric message template, metric to publish.
            Return value  : None.
        """
        metric_msg = self.mqtt_client.mqtt_message_factory.new_message(
            metric_template)
        metric_msg.set_metric(metric_dict)
        self.mqtt_client.publish_message(metric_msg)