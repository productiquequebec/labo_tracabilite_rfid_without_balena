from tracking_station.tests import test_runner
from tracking_station.station import TrackingStation
from tracking_station.config.config import ConfigType
import sys

def run_station(config_type):
    
    tracking_station = TrackingStation(config_type)
    
    try:
        ret = tracking_station.init()
        if ret is not None : raise RuntimeError(str(ret))
        tracking_station.run()
    except Exception as e:
        print("Exception raised: {}".format(str(e)))
    except KeyboardInterrupt:
        print("\nKeyboard Interrupt detected")
    finally:
        tracking_station.delete()

def run_tests():
    test_runner.run_tests()
    
if __name__ == "__main__":

    if len(sys.argv) < 2:
        raise SyntaxError("Missing required argument")

    if sys.argv[1] == 'test':
        run_tests()

    elif sys.argv[1] == 'run':
        if len(sys.argv) < 3:
            raise SyntaxError("Missing required argument")

        if sys.argv[2] == ConfigType.BALENA:
            run_station(ConfigType.BALENA)
        elif sys.argv[2] == ConfigType.JSON:
            run_station(ConfigType.JSON)
        else:
            raise SyntaxError("Invalid configuration type")
        
    else:
        raise SyntaxError("Invalid mode")


