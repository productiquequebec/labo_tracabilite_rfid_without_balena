echo "<<<Download linux_libnfc-nci repository>>>"
git clone https://github.com/NXPNFCLinux/linux_libnfc-nci.git
cd linux_libnfc-nci
echo "<<<Execute bootstrap>>>"
chmod +x bootstrap
./bootstrap
echo "<<<Execute configure>>>"
chmod +x configure
./configure --enable-alt
echo "<<<Make>>>"
make
echo "<<<Make install>>>"
sudo make install
echo "<<<Export library path>>>"
echo "export LD_LIBRARY_PATH=/usr/local/lib" >> .bashrc